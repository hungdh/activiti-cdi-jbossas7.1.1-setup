import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.activiti.engine.ProcessEngine;


@Startup
@Singleton
public class TestBean {
  
  private static Logger log = Logger.getLogger(TestBean.class.getName());
  
  @Inject 
  private ProcessEngine processEngine;
  
  @PostConstruct
  protected void init() {
    log.info("Process engine '"+processEngine.getName()+"' is up and running!");
  }

}
