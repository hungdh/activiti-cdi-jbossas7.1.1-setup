package config;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.TransactionManager;

import org.activiti.cdi.CdiJtaProcessEngineConfiguration;
import org.activiti.cdi.spi.ProcessEngineLookup;
import org.activiti.engine.ProcessEngine;

public class ProgrammaticProcessEngineConfiguration implements
		ProcessEngineLookup {

	private ProcessEngine processEngine;

	public int getPrecedence() {
		return 10;
	}

	private TransactionManager lookupTransactionManager() {
		try {
			return InitialContext.doLookup("java:jboss/TransactionManager");
		} catch (NamingException e) {
			throw new RuntimeException("Unable to lookup transaction manager",
					e);
		}
	}

	public ProcessEngine getProcessEngine() {
		CdiJtaProcessEngineConfiguration processEngineConfiguration = new CdiJtaProcessEngineConfiguration();
		processEngineConfiguration.setProcessEngineName("default");
		processEngineConfiguration
				.setDataSourceJndiName("java:jboss/datasources/ExampleDS");
		processEngineConfiguration
				.setTransactionManager(lookupTransactionManager());
		processEngineConfiguration.setDatabaseType("h2");
		processEngineConfiguration.setTransactionsExternallyManaged(true);
		processEngineConfiguration.setJobExecutorActivate(true);
		processEngine = processEngineConfiguration.buildProcessEngine();
		return processEngine;
	}

	public void ungetProcessEngine() {
		processEngine.close();
		processEngine = null;
	}

}
